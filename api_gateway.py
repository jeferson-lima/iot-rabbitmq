#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Flask
from flask_restplus import Resource, Api
from flask import request
import logging
from datetime import timedelta
import time as t
from iot import Actions
import os

# application info
app = Flask(__name__)
app.config["DEBUG"] = True
app.config["THREADED"] = False

api = Api(app,
        version='0.0.3', 
        title='IoT-RabbitMQ API Gateway',
        description='IoT-RabbitMQ HTTP API')

# port
PORT = int(os.getenv('PORT', 8080))
jsonpack = api.parser()
jsonpack.add_argument('json',location='json' ,required=True)

# API get argumments method
get_query = api.parser()
get_query.add_argument('etag', location='text', required=True)
get_query.add_argument('action', location='text', required=True)


# config logger
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s-%(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename='./iot_api.log',
                    filemode='w')
logger = logging.getLogger(__name__)

iot = api.namespace('iot', description='Device Application API')

@iot.route('/api/')
class IoTGateway(Resource):
    @iot.expect(jsonpack)
    def post(self):
        t0 = t.time()
        try:
            header = request.json['header']
            body   = request.json['body']
            action_eval = Actions(header, body)
            response = action_eval.select_actions()
            response.update({"api_timespan": t.time() - t0})
            return response
        except:
            # not a json valid
            header  = {"header":{"code":"001",
                      "api_timespan": t.time() - t0}}
            return header

if __name__ == '__main__':
    app.run(host='localhost', port = PORT)

