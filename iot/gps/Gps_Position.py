# IoT-RabbitMQ API Packege

from ..core.stdMessage import IoTMsg


class Gps_Device_Position(IoTMsg):
    """
    Parameters
    ----------
    Returns
    -------
    Raises
    ------
    """

    def __init__(self, body):
        super().__init__()
        self.device_id = body['device_id']

    def get_device_gps_pos(self):
        with self.services as rpc:
            value = rpc.device_gps_pos.get_value(self.device_id)
        return value
        