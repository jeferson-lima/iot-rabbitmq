# IoT RabbitMQ

## Documentation

https://jeferson.lima.gitlab.io/iot-rabbitmq/

## How to test it?

```bash
(project folder)$ sudo python3 -m pip install -r requirements
(project folder)$ python3 api_gateway.py
```

Start services:

```bash
(project folder)$ cd services/
(project folder)$ nameko run ticket_value_service
```

and make a api request:

```bash
curl -X POST "http://localhost:8080/iot/api/" -H "accept: application/json" -H "Content-Type: application/json" -d '{"header": {"action": "iotrabbitmq.api.ticket_value","etag": "a41d6cc6d92b52a9ae8f127b9f335bdac8af9dcebaf847c02ba489ff","code": "000","document_datetime_init": 123423424324},"body": {}}'
```

response:
```json
{
    "header": {
        "action": "iotrabbitmq.api.ticket_value",
        "etag": "a41d6cc6d92b52a9ae8f127b9f335bdac8af9dcebaf847c02ba489ff",
        "code": "000",
        "document_datetime_init": 123423424324
    },
    "body": {
        "Normal": 1.2,
        "Student": 2.2,
        "Elderly": 0
    },
    "api_timespan": 0.04354453086853027
}
```

## Software Requirements

* Message Broker: [RabbitMQ](https://www.rabbitmq.com/install-debian.html)


